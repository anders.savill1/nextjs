export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 3: How would you compare Next.js with another framework? Feel free to compare
        with a framework of your choice.</p></div>
        <div><p><br/>My experience with Next.js in a professional sense is limited to a small number of investigations in previous roles and
        working with the framework in the last week in preparation for this interview. Overall I have found that the new app router is a fantastic
        feature that is very similar to the way golang projects are structured.</p>
        <p><br/>In comparison I have some experience working with and configuring feathersjs. In my view Next.js is a framework focused on delivering 
        a highly scalable front end framework that is simple to use for basic projects but flexible enough to be an extremely powerful tool for those
        who have a higher level of competence. 
        </p><p><br/>Feathersjs on the other hand is also a powerful framework, however it is focused on compatibility with as many data sources
        as possible and providing an easy to use interface with front end applications. While Feathersjs has some very useful features which can
        be useful in the right hands it does not cover the broad scope that Next.js does. Additionally it would require a developer to learn the
        intricacies of the framework and run a separate middleware application to support it.</p></div>
        </div>
        </>
        )
}
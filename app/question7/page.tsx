export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 7: How could we improve or alter this familiarization exercise?</p></div>
        <div><p><br/>I think that this familiarisation exercise was excellent, I find that this type of interview for customer success focuses too much on the technical side
        of things and not enough on the customer engagement side of things. The only problem I had was knowing if it was acceptable to treat this exercise as &apos;open book&apos; or
        &apos;closed book&apos;</p></div>
        </div>
        </>
        )
}
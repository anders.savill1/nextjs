export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 1: What do you want to learn or do more of at work?</p></div>
        <div><p><br/>I would like to further learn and work on development projects. In previous customer success roles I either didn&apos;t have the time at work or the ability
            to work on these kinds of projects due to the demands of the role. I feel that strengthening my skills in this area can help me achive a personal goal of
            technical improvement in addition to building tools in a team environment that can be leveraged to automate regular tasks resulting in a faster time to resolution. </p></div>
        </div>
        </>
        )
}
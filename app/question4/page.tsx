export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 4: When would you choose to use Edge Functions, Serverless Functions, or Edge
Middleware with Vercel?</p></div>
        <div><p><br/>I would use Edge Middleware for any logic which can be defined as static rules. All Edge Middleware operations happen pre-cache and should
        be leveraged wherever possible to prevent requests from consuming usage quotas which in turn protects against overages</p>
        <p><br/>I would use Edge Functions to process requests that can&apos;t be processed by Edge Middleware and only require ephemeral data storage. 
        You can identify users against Oath or a global database object and store this information ephemerally 
        in a low latency fashion while preserving your Serverless Function quota.</p>
        <p><br/>I would use serverless functions for processing requests that require permanent storage or do not require the low latency of Edge functions.
        Generating long cache life objects, processing payments or any kind of complex task which is not suitable for Edge or Middleware functions</p>
        <p><br/>In summary there is a large amount of variables that should be considered when choosing the most appropriate service. This would be
        determined by the needs of the business and the application. In general every business is on a budget, therefore whichever combination of applications
        can be used to maximise the user experience without compromising the business goals becomes the most appropriate.</p>
        </div>
        </div>
        </>
        )
}
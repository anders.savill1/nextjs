export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 2: Describe how you solved a challenge that one of your previous support teams
faced. How did you determine your solution was successful?</p></div>
        <div><p><br/>In one of my previous support teams we were faced with an escalating number of site outage alerts confined to a specific partner. 
        Clusters would report in maintenance, however the configuration on the clusters would report not in maintenance mode. This type of issue was
        compounded by a gradual increase in outage alerts over time and in turn was not investigated and the team simply dealt with a larger volume of alerts.
        </p><p><br/>I worked with other team members to investigate this issue which turned out to be a new feature in the application which allowed end users to configure
        a maintanence flag at the CDN without setting the usual maitenance mode on the cluster.
        </p><p><br/>As this was a feature provided by the partner we developed an ad-hoc tool to query the CDN API and retrieve the maintenance flag.
        This tool was tested on production clusters and plans were put in place to impliment it in outage reports and monitoring tools.</p></div>
        </div>
        </>
        )
}
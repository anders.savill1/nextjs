export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 5: Imagine a customer writes in, requesting help with the following question. Write a
first response for triaging this case and helping them solve the issue.</p></div>
        <div><p><br/>Hi, <br/><br/>Can you please confirm the following for me:<br/>
        1. Any recent changes to your code that may affect this<br/>
        2. You are calling &apos;next build&apos; in your package.json<br/>
        3. In your vercel dashboard the Build Command is not overidden or that it calls &apos;next build&apos;<br/>
        4. In your vercel dashboard the Output Directory is not overridden<br/><br/>

        I understand your frustration and if none of these steps assist with your issues please provide me with the full output of your build log.<br/><br/>

        Best Regards<br/><br/>

        Anders<br/><br/>

        Note: I am unsure if this exercise is &apos;open book&apos;. When faced with an unfamiliar
        issue I would usually research the issue before responding and in this case that&apos;s what I have done.
        In addition assuming access, I would review anything I am able to in relation to the questions asked in my answer on behalf of the customer. (Including the build log)


        </p></div>
        </div>
        </>
        )
}
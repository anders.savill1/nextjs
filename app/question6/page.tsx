export default function Page() {

    return (
        <>
        <div className="h-screen flex-wrap flex items-center justify-center grid auto-rows-min mt-20 ml-20 w-2/4">
        <div><p>Question 6: What do you think is one of the most common problems which customers ask
Vercel for help with? How would you help customers to overcome common
problems, short-term and long-term?</p></div>
        <div><p><br/>I think one of the most common problems customers ask for help would be simply getting started. There is a wide array of
        services used by customers to get from code in a local environment to the Vercel service. They may have questions about git, git provider integrations,
        how to adapt their application to work with Vercel and how to best integrate and utilise Vercels bespoke data services.</p>
        <p><br/>In the short term I would help customers to overcome these problems by becoming a subject matter expert in all things Vercel with a goal of being able
        to provide as many first response resolutions as possible. I also believe it&apos;s important to explain to the customer how you arrived at the answer in an 
        effort to help them learn the platform better.</p>
        <p><br/>In the long term I believe that providing feedback on features and bugs is critical and this must be supported
             by contributing to internal and public facing documentation as much as possible.</p> 
 
        </div>
        </div>
        </>
        )
}